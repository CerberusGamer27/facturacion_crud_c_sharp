using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_Final
{
    class Program
    {
        static void Main(string[] args)
        {
            //int i, j;
            object[,] matrix = new object[10, 10];
            object[] datos = new object[3];
            int codigo = 0, x = 0, c = 0, cantidad = 0, bandera, k = 0, h = 0, seleccion = 0, seleccion2 = 0, seleccion3 = 0;
            int credito_fiscal;
            double precio = 0;
            double total = 0, sub_total = 0, iva = 0;
            string nombre_producto, nombre_cliente, fecha_modificacion;
            bool condicion = true;
            DateTime fecha = DateTime.Now;
            while (condicion == true)
            {
                Console.WriteLine("Bienvenido, ingrese la opcion que desea realizar: \n");
                Console.WriteLine("1) Facturar una Venta=> \n");
                Console.WriteLine("2) Modificar los datos de una venta=> \n");
                Console.WriteLine("3) Buscar en los registros una compra=> \n");
                Console.WriteLine("4) Salir=> \n");
                Console.Write("=============> ");
                seleccion = int.Parse(Console.ReadLine());
                Console.WriteLine(" ");
                Console.WriteLine("#############################\n");

                switch (seleccion)
                {
                    ///////////////////////////////////////////////////////////////
                    case 1://Ingresar una venta
                        Console.WriteLine("");
                        Console.WriteLine("Ingrese el tipo de cliente: \n");
                        Console.WriteLine("1) Credito Fiscal ");
                        Console.WriteLine("2) Solo Ticket\n");
                        Console.Write("=================> ");
                        seleccion2 = int.Parse(Console.ReadLine());
                        Console.WriteLine("\n");

                        Console.Write("Ingrese el codigo de la factura, por favor => ");
                        codigo = int.Parse(Console.ReadLine());
                        matrix[k, 0] = codigo;
                        Console.WriteLine("");

                        matrix[k, 1] = fecha;

                        Console.Write("Ingrese el nombre del cliente, por favor => ");
                        nombre_cliente = Console.ReadLine();
                        matrix[k, 2] = nombre_cliente;
                        Console.WriteLine("");

                        Console.Write("Ingrese el nombre del producto, por favor => ");
                        nombre_producto = Console.ReadLine();
                        matrix[k, 3] = nombre_producto;
                        Console.WriteLine("");

                        Console.Write("Ingrese la cantidad del producto, por favor => ");
                        cantidad = int.Parse(Console.ReadLine());
                        matrix[k, 4] = cantidad;
                        Console.WriteLine("");

                        Console.Write("Ingrese el precio del producto, por favor ($)=> ");
                        precio = double.Parse(Console.ReadLine());
                        matrix[k, 5] = precio;
                        Console.WriteLine("");

                        sub_total = (cantidad * precio);
                        matrix[k, 6] = sub_total;
                        if (seleccion2 == 1)
                        {
                            //iva = (0);
                            matrix[k, 7] = (0);

                            total = (sub_total);
                            matrix[k, 8] = total;

                            credito_fiscal = 1;
                            matrix[k, 9] = credito_fiscal;
                        }
                        else
                        {
                            iva = (sub_total * 0.13);
                            matrix[k, 7] = iva;

                            total = (sub_total + iva);
                            matrix[k, 8] = total;

                            credito_fiscal = 0;
                            matrix[k, 9] = credito_fiscal;
                        }


                        Console.WriteLine("#############################\n");
                        k++;

                        break;
                    ///////////////////////////////////////////////////////////////
                    case 2://Modificar los datos de una venta
                        //Console.WriteLine("as");
                        Console.Write("Ingrese el codigo del articulo=> ");
                        c = int.Parse(Console.ReadLine());
                        Console.WriteLine("\n");
                        bandera = -1;

                        for (x = 0; x <= 9; x++)
                        {
                            if (c == Convert.ToInt32(matrix[x, 0]))
                            {
                                bandera = x;
                                break;
                            }
                        }
                        if (bandera < 0)
                        {
                            Console.WriteLine("El articulo no existe");
                            Console.WriteLine("\n");
                            Console.WriteLine("#############################\n");
                        }
                        else
                        {
                            //Console.WriteLine("Fsg");
                            Console.WriteLine("Ingrese el dato que desea modificar: \n");
                            Console.WriteLine("1) Fecha");
                            Console.WriteLine("2) Nombre del Cliente");
                            Console.WriteLine("3) Nombre del Producto");
                            Console.WriteLine("4) Cantidad del Producto");
                            Console.WriteLine("5) Precio del Producto\n");
                            Console.Write("=========================> ");
                            seleccion3 = int.Parse(Console.ReadLine());
                            Console.WriteLine("\n");
                            switch (seleccion3)
                            {
                                case 1:
                                    Console.WriteLine("Ha Seleccionado hacer Cambio en la Fecha: \n");
                                    Console.Write("Por favor ingrese la nueva fecha, en este formato dd/dd/yyyy=> ");
                                    fecha_modificacion = Console.ReadLine();
                                    fecha = DateTime.Parse(fecha_modificacion);
                                    matrix[bandera, 1] = fecha;
                                    break;
                                case 2:
                                    Console.WriteLine("Ha Seleccionado hacer Cambio de Nombre del Cliente: \n");
                                    Console.Write("Por favor ingrese el nuevo nombre del Cliente => ");
                                    nombre_cliente = Console.ReadLine();
                                    matrix[bandera, 2] = nombre_cliente;
                                    break;
                                case 3:
                                    Console.WriteLine("Ha Seleccionado hacer Cambio de Nombre del Producto: \n");
                                    Console.Write("Por favor ingrese el Nuevo Nombre del Producto => ");
                                    nombre_producto = Console.ReadLine();
                                    matrix[bandera, 3] = nombre_producto;
                                    break;
                                case 4:
                                    Console.WriteLine("Ha Seleccionado hacer Cambio en Cantidad del Producto: \n");
                                    Console.Write("Por favor ingrese la nueva Cantidad del Producto => ");
                                    cantidad = int.Parse(Console.ReadLine());
                                    matrix[bandera, 4] = cantidad;

                                    if (Convert.ToInt32(matrix[bandera, 9]) == 1)//Si existe credito fiscal
                                    {
                                        sub_total = (cantidad * Convert.ToDouble(matrix[bandera,5]));
                                        matrix[bandera, 6] = sub_total;

                                        matrix[k, 7] = (0);

                                        total = (sub_total);
                                        matrix[bandera, 8] = total;

                                    }
                                    else if (Convert.ToInt32(matrix[bandera, 9]) == 0)
                                    {
                                        sub_total = (cantidad * Convert.ToDouble(matrix[bandera, 5]));
                                        matrix[bandera, 6] = sub_total;

                                        iva = (sub_total * 0.13);
                                        matrix[bandera, 7] = iva;

                                        total = (sub_total + iva);
                                        matrix[bandera, 8] = total;

                                        credito_fiscal = 0;
                                        matrix[bandera, 9] = credito_fiscal;
                                    }
                                    break;
                                case 5:
                                    Console.WriteLine("Ha Seleccionado hacer Cambio en Precio del Producto: \n");
                                    Console.Write("Por favor ingrese el nuevo Precio  del Producto($) => ");
                                    precio = double.Parse(Console.ReadLine());
                                    matrix[bandera, 5] = precio;
                                    if (Convert.ToInt32(matrix[bandera, 9]) == 1)//Si existe credito fiscal
                                    {
                                        sub_total = (precio * Convert.ToDouble(matrix[bandera, 4]));
                                        matrix[bandera, 6] = sub_total;

                                        matrix[bandera, 7] = (0);

                                        total = (sub_total);
                                        matrix[bandera, 8] = total;

                                    }
                                    else if (Convert.ToInt32(matrix[bandera, 9]) == 0)
                                    {
                                        sub_total = (precio * Convert.ToDouble(matrix[bandera, 4]));
                                        matrix[bandera, 6] = sub_total;

                                        iva = (sub_total * 0.13);
                                        matrix[bandera, 7] = iva;

                                        total = (sub_total + iva);
                                        matrix[bandera, 8] = total;

                                        credito_fiscal = 0;
                                        matrix[bandera, 9] = credito_fiscal;
                                    }
                                    break;
                            }

                        }
                        break;
                    ///////////////////////////////////////////////////////////////
                    case 3://Buscar una venta en los registros
                        //************************************//
                        for (int filas = 0; filas < matrix.GetLength(0); filas++)
                        {
                            for (int columnas = 0; columnas < matrix.GetLength(1); columnas++)
                            {
                                Console.Write(matrix[filas, columnas] + "\t");
                            }

                            Console.WriteLine("\n");
                        }
                        //************************************//
                        Console.Write("Ingrese el codigo del articulo=> ");
                        c = int.Parse(Console.ReadLine());
                        Console.WriteLine("\n");
                        bandera = -1;

                        for (x = 0; x <= 9; x++)
                        {
                            if (c == Convert.ToInt32(matrix[x, 0]))
                            {
                                bandera = x;
                                break;
                            }
                        }
                        if (bandera < 0)
                        {
                            Console.WriteLine("El articulo no existe");
                            Console.WriteLine("\n");
                            Console.WriteLine("#############################\n");
                        }
                        else
                        {
                            Console.WriteLine("El articulo existe: Codigo: " + matrix[bandera, 0] + " || Fecha: " + matrix[bandera, 1] + " || Sub Total: " + matrix[bandera, 6] + " || IVA: " + matrix[bandera, 7] + " || Total: " + matrix[bandera, 8]);
                            Console.WriteLine("\n");
                            Console.WriteLine("#############################\n");
                        }
                        //************************************//
                        //Console.WriteLine(p);

                        break;
                    ///////////////////////////////////////////////////////////////
                    case 4:
                        Console.WriteLine("Adios, Gracias por prefrerirnos :D");
                        condicion = false;
                        break;

                    default:
                        Console.WriteLine("A ingresado una opcion no valida, por favor intente de nuevo");
                        break;
                }
                //Console.WriteLine(matrix.Length);
            }
            Console.ReadLine();

        }
    }
}